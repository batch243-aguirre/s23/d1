// console.log("Hello World!");

// [Section] Objects
	/*
		- An object is a data type that is used to represent real world objects.
		- It is a collection of related data and/or functionalites/method.
		- Information stored in objects are represented in a "key:value" pair
		-Key is also mostly referred to as "property" of an object.
		-Different data tyoe may be stored in an object's property creating data structures.
	*/

// Creating objects using object initializers/ literal notation

/*
	syntax:
		let objectName = {
			keyA: valueA,
			keyB: valueB
		}
	- This creates/declares an object and also initializes/assign it's properties upon creation.
	- A cellphone is an example of real world object
	-It has its own properties such as name, color, weigth , unit model and lot of other properties
*/

let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999
}

console.log("Result from creating using literal notaion: ")
console.log(cellphone);


// Creating objects using constructor function

/*
	-creates a reusable function to create several objects that have the same data structure
	-This is useful for creating multiple instances/copies of an object
	-An instance is a concreate occurence of any object which emphaszie distinc/unique identity of it
	-syntax:
	function objectName(valueA, valueB){
		this.keyA = valueA,
		this.keyB = valueB}
*/
	/*This is an constructor function*/
	/* this keyword allows us to assign a new object's properties by associating them with values received from a constructor function's parameter*/
	
	function Laptop(name, manufactureDate, ram){
		this.laptopName = name;
		this.laptopManufactureDate = manufactureDate;
		this.laptopRam = ram;
	}

	// Instatiation
		// The "new" operator creates an instances of an object
		// Objects and instances are often interchange because objecct literals(let object = {}) and instances (let objectName = new functionName(arguments)) are distinct/unqiue objects

	let laptop = new Laptop('Lenovo', 2008, "2 gb");
	console.log("Result from creating objects using object Construtor: ");
	console.log(laptop);

	let myLaptop = new Laptop("MacBook Air", 2020, "8 gb");
	console.log("Result from creating objects using object Construtor: ");
	console.log(myLaptop);

	let oldLaptop = new Laptop("Portal R2E CCMC", 1980, "500 mb");
		/*
			The example above invoke/calls the laptop function insted of creating a new object
			return "undefined" without the "new" operator because the "laptop" function does not have any return statement
		*/
	console.log("Result from creating objects with the new keyword: ")
	console.log(oldLaptop);

	/*Mini-activity*/
	// You will create a constructor function that will let us instatiate a new object, Menu, property : menuName, menuPrice.

	function Menu(mealName, mealPrice){
		this.menuName = mealName;
		this.menuPrice= mealPrice;
	}

	let mealOne = new Menu("Breakfast", 299);
	console.log(mealOne);

	// creatng empty objects
	let computer = {};
	let myComputer = new Object();
	console.log(computer);
	console.log(myComputer);

	// Accessing objects inside an array;

	let array = [laptop, myLaptop]
	console.log(array);
	console.log(array[0]);
	/*dot notation*/
	console.log(array[0].laptopManufactureDate);

	console.log(laptop);
	console.log(laptop.laptopName);

	// [Section] Initializing/adding/deleting/reassigning Object properties.
	/*
		-like any other variable in Javasript, objecyts have their properties initialized/ added after the object was created/ declared.
	*/


	let car ={};
	console.log(car);
	// Initializing/ adding object properties using dot notation

	car.name = 'Honda Civic';

	console.log(car);

	// Intializing/adding object property using bracket notation

	car['manufactureDate'] = 2019;

	console.log(car);

	// deleting object properties
		// deleting using bracket notation
	/*delete car["name"];
	console.log(car);*/
		//deleting property using dot notation
	delete car.manufactureDate;
	console.log(car);

	// reassigning object properties
		// reassign object- dot notation
		car.name = "Dodge Charger R/T";
		console.log(car)
		// reassing object property - bracket Notation
		car["name"] = "Jeepney";
		console.log(car);

	// [Section] Object Methods
		// A method is a function which is a property of an object.
		// They are also functions and one of tthe key differences they have is that methods are functions related to a specific object

	let person = {
		name: 'Chris',
		talk: function(){
			console.log("Hello my name is " + this.name)
		}
	}

	console.log(person);

	person.talk();

	// add method to objects
	person.walk = function(){
		console.log(this.name + " walked 25 steps forward.")
	};

	person.walk();

	// methods are useful for creating reusable functions that perform tasks related to objects.
	let friends = {
		firstName: 'Joe',
		lastName: 'Smith',
		address: {
			city: 'Austin',
			country: 'Texas'
		},
		phoneNumber: [['09123456789'],['043-4214-321']],
		emails: ['joe@mail.com', 'joesmith@email.xyz'],
		introduce: function(){
			console.log("Hello my name is " + this.firstName + " " + this.lastName + ". I live in " +this.address.city + " " + this.address.country + " . My emails are " + this.emails[0] + " and " + this.emails[1] + ". My numbers are " + this.phoneNumber[0][0] + " and " + this.phoneNumber[1][0])
		}
	}

	friends.introduce();

	/*create an object constructor*/

	function Pokemon(name, level){
		// Properties Pokemon
		this.pokemonName = name;
		this.pokemonLevel = level;
		this.pokemonHealth = 2 * level;
		this.pokemonAttack = level;

		/*methods*/
		/*we are going to add a method named tackle*/
		this.tackle = function(targetPokemon){
			console.log(this.pokemonName + " tackles " +  targetPokemon.pokemonName);
			console.log("targetPokemn's health is now reduced to _targetPokemonHealth_")

		}

		this.fainted = function(){
			console.log(this.pokemonName + " fainted!");
		}


	}

	let pikachu = new Pokemon("Pikachu", 12);
	console.log(pikachu);
	let gyarados = new Pokemon("Gyarados", 20);
	console.log(gyarados);
	pikachu.tackle(gyarados);
	gyarados.fainted();
